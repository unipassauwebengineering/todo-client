app.controller('todoController', ['$scope', '$state', '$stateParams', '$location', '$timeout', '$window', 'GroupService', 'TodoService', 'anchorSmoothScroll', function($scope, $state, $stateParams, $location, $timeout, $window, GroupService, TodoService, anchorSmoothScroll) {
	var params = angular.copy($stateParams);

	var id = $stateParams.groupId;
	$scope.showAddTodo = id < 0;
	if(id > 0) {
		GroupService.get(id).then(function(response){
			$scope.group = response.data;
			$scope.leftTitle = function(){
	    		return 50 - $scope.group.name.length;
	    	};
	    	
	    	$scope.leftDescription = function() {
	    		return 150 - $scope.group.description.length;
	    	};
		

	    });
	} else if(id == 0) {
		$scope.group = {id: 0, name: "Ohne Gruppe", description: "Hier finden Sie alle Todos, die keiner Gruppe zugordnet sind."};
		$scope.leftTitle = function(){
    		return 50 - $scope.group.name.length;
    	};
    	
    	$scope.leftDescription = function() {
    		return 150 - $scope.group.description.length;
    	};
	} else {
		$scope.group = {id: -1, name: "", description: ""};
		$scope.leftTitle = function(){
    		return 50 - $scope.group.name.length;
    	};
    	
    	$scope.leftDescription = function() {
    		return 150 - $scope.group.description.length;
    	};
	}
    TodoService.list(id).then(function(response) {
        $scope.todos = response.data;
		$scope.todoCount = response.data.length;
        initTodoCarousel($scope, $window);

        $timeout(function() {
            $location.hash("anchorGroup");
            anchorSmoothScroll.scrollTo("anchorGroup");
        }, 800);
    });
    
    $scope.save = function() {
    	GroupService.save(JSON.stringify($scope.group)).then(function (response) {
			// Success
    		if(response.data.code == 1) {
    			showSaveNote(true, "Gespeichert");
                $state.transitionTo("group", params, { reload: true, inherit: true, notify: true });
    		} else {
    			showSaveNote(false, "Fehler beim Speichern der Gruppe \"" + $scope.group.title + "\".");
    			$log.log(response.data.message)
    		};
		});
    };
    
    $scope.remove = function() {
        if(confirm("Wollen Sie dieses Todo wirklich löschen? Diese Änderung kann nicht rückgängig gemacht werden.")){
        	GroupService.remove($scope.group.id).then(function (response) {
    			// Success
        		if(response.data.code == 1) {
        			showSaveNote(true, "Gelöscht");
                    $state.transitionTo("group", params, { reload: true, inherit: true, notify: true });
        		} else {
        			showSaveNote(false, "Fehler beim Löschen.");
        			$log.log(response.data.message)
        		};
    		});
        };
    };
    
    $scope.close = function() {
        $state.transitionTo("group", params, { reload: true, inherit: true, notify: true });
    };
}]);

function initTodoCarousel($scope, $window) {
	// Breite des Window ermitteln
	var breite = $window.innerWidth;

	// Init Carousel
	$scope.myInterval = 0;
	$scope.displayItems = 1;
	var i, first = [], second, third;

	var many = Math.floor(breite / 500);

	if (many < 1) {
		many = 1;
	}

	$scope.displayItems = many;

	if ($scope.todos[i] != null) {
		for (var i = 0; i < $scope.todos[i].length; i++) {
			$scope.todos[i].active = true;
		}
	}

	for (i = 0; i < $scope.todos.length; i += many) {
		second = {
			entry1 : $scope.todos[i]
		};
		if (many == 1) {
		}
		if ($scope.todos[i + 1] && (many == 2 || many == 3)) {
			second.entry2 = $scope.todos[i + 1]

		}
		if ($scope.todos[i + (many - 1)] && many == 3) {
			second.entry3 = $scope.todos[i + 2]
		}
		first.push(second);
	}
	$scope.todoSlides = first;
}

app.controller('todoDetailController', ['$scope', '$stateParams', '$state', '$location', '$filter', '$log', 'anchorSmoothScroll', 'TodoService', 'GroupService', function($scope, $stateParams, $state, $location, $filter, $log, anchorSmoothScroll, TodoService, GroupService) {
	var params = angular.copy($stateParams);
	
	TodoService.get($stateParams.todoId).then(function(response){
		$scope.todo = response.data;
		$scope.leftTitle = function(){
			if($scope.todo.title != null) {
				return 50 - $scope.todo.title.length;
			} else {
				return 50;
			}
		};
		
		$scope.leftDescription = function() {
			if($scope.todo.description != null) {
				return 150 - $scope.todo.description.length;
			} else {
				return 150;
			}
		};
	});
	
	GroupService.list().then(function(response){
		$scope.groups = response.data;
	    $scope.selectedGroup = ($filter('filter')($scope.groups, {id: $stateParams.groupId}))[0];
	});
	
    $scope.save = function() {
    	if($scope.selectedGroup.id != 0) {
	    	$scope.todo.group = $scope.selectedGroup;
	    } else {
	    	$scope.todo.group = null;
	    }
    	TodoService.save(JSON.stringify($scope.todo)).then(function (response) {
			// Success
    		if(response.data.code == 1) {
    			showSaveNote(true, "Gespeichert");
                $state.transitionTo("group.detail", params, { reload: true, inherit: true, notify: true });
    		} else {
    			showSaveNote(false, "Fehler beim Speichern von Todo-Note \"" + $scope.todo.title + "\".");
    			$log.log(response.data.message)
    		};
		});
    };
    
    $scope.remove = function() {
        if(confirm("Wollen Sie dieses Todo wirklich löschen? Diese Änderung kann nicht rückgängig gemacht werden.")){
        	TodoService.remove($scope.todo.id).then(function (response) {
    			// Success
        		if(response.data.code == 1) {
        			showSaveNote(true, "Gelöscht");
                    $state.transitionTo("group.detail", params, { reload: true, inherit: true, notify: true });
        		} else {
        			showSaveNote(false, "Fehler beim Löschen.");
        			$log.log(response.data.message)
        		};
    		});
        };
    };
    
    $scope.close = function() {
        $state.transitionTo("group.detail", params, { reload: true, inherit: true, notify: true });
    };
    
    $location.hash("anchorTodo");
    anchorSmoothScroll.scrollTo("anchorTodo");
}]);