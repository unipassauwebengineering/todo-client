app.controller('groupController', [
		'$scope',
		'$window',
		'$timeout',
		'GroupService',
		'ConnectionService',
		function($scope, $window, $timeout, GroupService, ConnectionService) {
			var w = angular.element($window);
			w.bind('resize', function() {
				$window.location.reload();
			});
	        $timeout(function(){
				ConnectionService.test().error(
						function(data, status, headers, config) {
							showSaveNote(0, "Keine Verbindung zum Webservice.");
						}).then(
						function(response) {
							if (response.data.code == 0) {
								showSaveNote(false,
										response.data.message);
							}
							GroupService.list().then(function(response) {
								$scope.groups = response.data;
								initCarousel($scope, $window);
							});
						});
	        }, 800);
		} ]);

function initCarousel($scope, $window) {
	// Breite des Window ermitteln
	var breite = $window.innerWidth;

	// Init Carousel
	$scope.myInterval = 0;
	$scope.displayItems = 1;
	var i, first = [], second, third;

	var many = Math.floor(breite / 500);

	if (many < 1) {
		many = 1;
	}

	$scope.displayItems = many;

	if ($scope.groups[i] != null) {
		for (var i = 0; i < $scope.groups[i].length; i++) {
			$scope.groups[i].active = true;
		}
	}

	for (i = 0; i < $scope.groups.length; i += many) {
		second = {
			entry1 : $scope.groups[i]
		};
		if (many == 1) {
		}
		if ($scope.groups[i + 1] && (many == 2 || many == 3)) {
			second.entry2 = $scope.groups[i + 1]

		}
		if ($scope.groups[i + (many - 1)] && many == 3) {
			second.entry3 = $scope.groups[i + 2]
		}
		first.push(second);
	}
	$scope.groupSlides = first;
}