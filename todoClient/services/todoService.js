app.factory("TodoService", [ "$http", function($http) {
	return {
		get: function(id) {
			if(id != 0) {
				return $http.get(getUrl + "note/" + id);
			}
		},
		list: function(id) {
			if(id == 0) {
				return $http.get(getUrl + "note/list");
			}
			return $http.get(getUrl + "note/list?id=" + id);
		},
		save: function(noteToSave) {
			return $http.post(getUrl + "note/save" , noteToSave);
		},
		remove: function(id) {
			return $http.delete(getUrl + "note/delete/" + id);
		}
	};
}]);