app.factory("GroupService", [ "$http", function($http) {
	return {
		get: function(id) {
			return $http.get(getUrl + "group/" + id);
		},
		list: function() {
			return $http.get(getUrl + "group/list");
		},
		save: function(groupToSave) {
			return $http.post(getUrl + "group/save" , groupToSave);
		},
		remove: function(id) {
			return $http.delete(getUrl + "group/delete/" + id);
		}
	};
}]);