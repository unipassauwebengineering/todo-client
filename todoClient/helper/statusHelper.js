function showSaveNote(success, message) {
	$("#infoBox").hide(0);
	$("#infoBox").html("<p>" + message + "</p>");
	if (success) {
		$("#infoBox").removeClass("failure").addClass("success").show(500).delay(1000).fadeOut(500);
	} else {
		$("#infoBox").removeClass("success").addClass("failure").show(500).delay(1500).fadeOut(500);
	}
}